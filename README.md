# High Current Power PQ

High Current Power PQ (HCPPQ) was primarily designed as a drop-in replacement for RMC's power systems and can be used in various PQ bus systems. The board is It is composed of two switching regulators (MP1477H) providing 3.3V and 5V from input from 4.2V to 17V and two LDO regulators providing 3.3V (NCP161ASN330T1G) and 5V (LM1117MPX-50NOPB) for analog supply. 